﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using CourseLogicLayer.Dtos;
using Homework1;
using Homework1.Services;
using HomeworkPart2.Forms;

namespace HomeworkPart2
{
    public class MainMenuOperations
    {

        public void MainMenu()
        {
            bool _isProgramWorking = true;

            while (_isProgramWorking)
            {
                Console.WriteLine("Dodaj studenta - AS" +
                                  "\nZmien dane Studenta - US" +
                                  "\nDodaj kurs - AC " +
                                  "\nWybierz istniejacy kurs - SC " +
                                  "\nWyjscie z programu - E"
                                  );
                string operation = Console.ReadLine();

                if (operation == "AS")
                {
                    AddStudentOperation();
                }
                else if (operation == "AC")
                {
                    AddCourseOperation();
                }
                else if (operation == "US")
                {
                    UpdateStudentOperation();
                }
                else if (operation == "SC")
                {
                    CourseDto course = SelectCourseOperation();
                    if (course != null)
                    {
                        CourseOperationMenu(course);
                    }
                }
                else if (operation == "E")
                    _isProgramWorking = false;
                else
                {
                    Console.WriteLine("Bledna komenda, sproboj ponownie");
                }
            } 
        }

        private void UpdateStudentOperation()
        {
            var pesel = ConsoleInput.GetPesel("Podaj pesel studenta");
            var service = new StudentService();
            var student = service.GetStudentDtoByPesel(pesel);
            if (student == null)
            {
                Console.WriteLine("Nie znalazlem takiego studenta.");
                return;
            }

            Console.WriteLine("Podaj nowe imie studenta");
            var input = Console.ReadLine();
            if (input.Length > 0)
            {
                student.Name = input;
            }

            Console.WriteLine("Podaj nowe nazwisko studenta");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                student.Surname = input;
            }
                
            Console.WriteLine("Podaj nowa date urodzin studenta");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                try
                {
                    var datetime = DateTime.Parse(input);
                    student.Birthday = datetime;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Zły format daty. Data nie zostanie zmodyfikowana..");
                }
            }

            string removeFromCourse = ConsoleInput.GetYesNo("Czy chcesz wypisac tego kursanta z kursu? (tak/nie)");
            if (removeFromCourse == "tak")
            {
                foreach (var course in student.Courses)
                {
                    Console.WriteLine(course.Id+". "+course.CourseName+" ("+course.TrainerName+")");
                }
                var courseId = ConsoleInput.GetInt("Podaj identyfikator kursu");
                var selectedCourseDto = student.Courses.First(c => c.Id == courseId) ?? null;
                student.RemoveFromCourse(selectedCourseDto);
            }

            service.UpdateStudent(student);
        }

        private void CourseOperationMenu(CourseDto course)
        {
            var operation = new CourseOperationMenu(course);
            operation.ShowMenu();
        }

        private CourseDto SelectCourseOperation()
        {
            try
            {
                var courseService = new CourseService();
                List<CourseDto> list = courseService.GetAllCourses();
                Console.WriteLine("Wybierz kurs:");
                for (int i = 0; i < list.Count; i++)
                {
                    var course = list[i];
                    Console.WriteLine(i + ". " + course.CourseName + " (" + course.TrainerName + ")");
                }
                int courseIndex = ConsoleInput.GetInt("Podaj numer kursu");
                Console.WriteLine("Wybrano kurs" + list[courseIndex].CourseName);
                return list[courseIndex];
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            
        }

        private void AddCourseOperation()
        {
            StudentService ss = new StudentService();
            if (ss.CountStudents() == 0)
            {
                Console.WriteLine("Najpierw dodaj studentow!");
                return;
            }

            CourseDto courseDto = new CourseDto();

            Console.WriteLine("Dziennik kursu, wprowadź dane: \nPodaj nazwę kursu");
            courseDto.CourseName = Console.ReadLine();

            Console.WriteLine("Podaj imię oraz nazwisko Prowadzącego");
            courseDto.TrainerName = Console.ReadLine();

            courseDto.CourseStartDate = ConsoleInput.GetDateTime("Podaj date rozpoczęcia kursu [dd/mm/yyyy]");

            courseDto.HomeworkThreshold =
                ConsoleInput.GetInt("Podaj próg punktowy zaliczenia prac domowych w procentach (bez wpisywania %)");

            courseDto.PresenceThreshold =
                ConsoleInput.GetInt("Podaj próg punktowy zaliczenia obecnosci (bez wpisywania %)");

            courseDto.AmountOfStudents = ConsoleInput.GetInt("Podaj liczbę kursantow");

            try
            {
                var cs = new CourseService();
                cs.AddCourse(courseDto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void AddStudentOperation()
        {
            try
            {
                StudentDto s = NewStudentForm.ShowForm();
                var studentService = new StudentService();
                studentService.AddStudent(s);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}