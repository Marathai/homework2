﻿using System;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;

namespace HomeworkPart2.Forms
{
    public class RaportForm
    {

        public void ShowRaport(CourseDto course)
        {
            Console.WriteLine(course.CourseName);
            Console.WriteLine(course.CourseStartDate);
            Console.WriteLine(course.PresenceThreshold);
            Console.WriteLine(course.HomeworkThreshold);

            int maxPoints = 0;
            foreach (var homework in course.Homeworks)
            {
                maxPoints += homework.RequiredPoints;
            }

            Console.WriteLine("Lista obecności");

            foreach (var student in course.Students)
            {
                int studentPersinstece = course.StudentPresence(student);
                float percentages = CalculatingManagment.PercentageCalculation(studentPersinstece, course.ClassDays.Count);

                if (percentages >= course.PresenceThreshold)
                    Console.WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPersinstece + "/" + course.ClassDays.Count + " (" +
                                      percentages + "%) - Zaliczone");
                else if (percentages < course.PresenceThreshold)
                    Console.WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPersinstece + "/" + course.ClassDays.Count + " (" +
                                      percentages + "%) - Niezaliczone");
                else
                    Console.WriteLine("Brak danych do wyswietlenia");
            }

            Console.WriteLine("Raport z prac domowych");
            foreach (var student in course.Students)
            {
                var studentPoints = course.StudentPoints(student);
                float percentages = CalculatingManagment.PercentageCalculation(studentPoints, maxPoints);

                if (percentages >= course.HomeworkThreshold)
                    Console.WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPoints + "/" + maxPoints + " (" +
                                      percentages + "%) - Zaliczone");
                else if (percentages < course.HomeworkThreshold)
                    Console.WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + " " +
                                      studentPoints + "/" + maxPoints + " (" +
                                      percentages + "%) - Niezaliczone");
                else
                    Console.WriteLine("Brak danych do wyswietlenia");
            }
        }
    }
}