﻿using System;
using CourseLogicLayer;
using CourseLogicLayer.Dtos;
using Homework1;
using Homework1.Services;
using HomeworkPart2.Forms;

namespace HomeworkPart2
{
    public class CourseOperationMenu
    {
        private CourseDto course = null;

        public CourseOperationMenu(CourseDto course)
        {
            this.course = course;
        }

        public void ShowMenu()
        {
            while (true)
            {
                Console.WriteLine(
                    "Co chcesz zrobić? " +
                    "\n " +
                    "\nDodaj studenta - DS; " +
                    "\nUsun studenta z kursu - USK " +
                    "\nDodać dzień zajec - DDZ; " +
                    "\nDodać prace domową - DPD; " +
                    "\nWypisać raport - R; " +
                    "\nEdytuj dane kursu - E " +
                    "\nZakończyć - exit");

                var operations = Console.ReadLine();

                if (operations == "DS")
                {
                    DSOption();
                }
                else if (operations == "DDZ")
                {
                    DDZOption();
                    operations = "DDZ";
                }
                else if (operations == "USK")
                {
                    USKOption();
                    operations = "USK";
                }
                else if (operations == "E")
                {
                   EditCourseOption();
                    operations = "E";
                }
                else if (operations == "DPD")
                {
                    DPDOperation();
                    operations = "DPD";
                }
                else if (operations == "R")
                {
                    ROperation();
                }
                else if (operations == "exit")
                {
                    break;
                }
                else if ((operations != "exit") && (operations != "R") && (operations != "E") && (operations != "DPD") 
                    && (operations != "USK") && (operations != "DDZ"))
                {
                    Console.WriteLine("Nieprawidłowa komenda, podaj co chcesz zrobic ponownie");
                }
                    
            }
        }

        private void EditCourseOption()
        {
            var service = new CourseService();

            Console.WriteLine("Podaj nowa nazwe");
            var input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.CourseName = input;
            }

            Console.WriteLine("Podaj nowe dane prowadzacego");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.TrainerName = input;
            }

            Console.WriteLine("Podaj nowy prog punktowy dla pracy domowej");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.HomeworkThreshold = Int32.Parse(input);
            }

            Console.WriteLine("Podaj nowy prog punktowy dla obecnosci");
            input = Console.ReadLine();
            if (input.Length > 0)
            {
                course.PresenceThreshold = Int32.Parse(input);
            }
            service.UpdateCourse(course);
        }

        private void USKOption()
        {
            Pesel p = ConsoleInput.GetPesel("Podaj pesel studenta");
            StudentService ss = new StudentService();
            StudentDto studentDto = ss.GetStudentDtoByPesel(p);

            if (studentDto != null)
            {
                course.RemoveStudentFromCourse(studentDto);

            }
            else
            {
                Console.WriteLine("Nie znalazlem takiego studenta");
            }
        }

        private void DSOption()
        {
            Pesel p = ConsoleInput.GetPesel("Podaj pesel studenta");
            StudentService ss = new StudentService();
            StudentDto studentDto = ss.GetStudentDtoByPesel(p);
            
            if (studentDto != null)
            {
                course.AddStudentToCourse(studentDto);

            }
            else
            {
                Console.WriteLine("Nie znalazlem takiego studenta");
            }
        }

        private void ROperation()
        {
            var raport = new RaportForm();
            raport.ShowRaport(course);
        }

        private void DPDOperation()
        {
            string operations;
            var reqPoints = ConsoleInput.GetInt("Podaj maksymalna ilość punktów do uzyskania");
            HomeworkDto homework = new HomeworkDto(reqPoints);

            foreach (var student in course.Students)
            {
                Console.WriteLine("Podaj punkty uzyskane przez " + student.Name + " " + student.Surname);
                operations = Console.ReadLine();
                homework.EarnedPoints.Add(student, Int32.Parse(operations));
            }
            course.Homeworks.Add(homework);
        }

        private void DDZOption()
        {
            string operations;
            ClassDayDto classDay = new ClassDayDto();
            classDay.ClassDate = ConsoleInput.GetDateTime("Podaj date zajęć [dd/mm/yyyy]");
            course.ClassDays.Add(classDay);

            foreach (var student in course.Students)
            {
                operations = ConsoleInput.GetYesNo("Czy " + student.Name + " " + student.Surname + " był obecny(a)? (tak/nie)");

                if (operations == "tak")
                {
                    classDay.Presense.Add(student, true);
                }
                if (operations == "nie")
                {
                    classDay.Presense.Add(student, false);
                }
            }
        }
    }
}