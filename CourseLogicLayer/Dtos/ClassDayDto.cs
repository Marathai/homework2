﻿using System;
using System.Collections.Generic;

namespace CourseLogicLayer.Dtos
{
    public class ClassDayDto
    {
        public int Id;
        public DateTime ClassDate;
        public Dictionary<StudentDto, bool> Presense= new Dictionary<StudentDto, bool>();

    }
}
