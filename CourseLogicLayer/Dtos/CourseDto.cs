﻿using System;
using System.Collections.Generic;
using System.Linq;
using Homework1.Services;

namespace CourseLogicLayer.Dtos
{
    public class CourseDto
    {
        public int Id;
        public string CourseName;
        public string TrainerName;
        public DateTime CourseStartDate;
        public int HomeworkThreshold;
        public int PresenceThreshold;

        public List<StudentDto> Students;
        public List<HomeworkDto> Homeworks;
        public List<ClassDayDto> ClassDays;

        public int AmountOfStudents;
        public int AmountOfCourseDays;

        public CourseDto()
        {
            Students = new List<StudentDto>();
            Homeworks = new List<HomeworkDto>();
            ClassDays = new List<ClassDayDto>();
        }

        public int StudentPresence(StudentDto student)
        {
            return ClassDays.Count(day => day.Presense[student]);
        }

        public int StudentPoints(StudentDto student)
        {
            int points = 0;
            foreach (var homeworkDto in Homeworks)
            {
                points += homeworkDto.getStudentPoints(student);
            }
            return points;
        }

        public void AddHomework(HomeworkDto homework)
        {
            Homeworks.Add(homework);
        }

        public void AddStudentToCourse(StudentDto student)
        {
            try
            {
                var service = new CourseService();
                Students.Add(student);
                service.AddStudentToCourse(student, this);
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("Operacja zakonczona niepowodzeniem");
            }
        }

        public void RemoveStudentFromCourse(StudentDto student)
        {
            try
            {
                var service = new CourseService();
                Students.Remove(student);
                service.RemoveStudentFromCourse(student, this);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Operacja zakonczona niepowodzeniem");
            }
        }
    }
}