﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseLogicLayer.Dtos;
using DataLayer.Repositories;
using Homework1.Mappers;

namespace Homework1.Services
{
    public class CourseService
    {
        private CourseDto _currentCourseDto = null;

        public void AddCourse(CourseDto courseDto)
        {
            var course = FromDtoToEntityMapper.CourseDtoToEntity(courseDto);
            var courseRepo = new CourseRepository();
            courseRepo.AddCourse(course);
        }

        public void UpdateCourse(CourseDto courseDto)
        {
            var course = FromDtoToEntityMapper.CourseDtoToEntity(courseDto);
            var repo = new CourseRepository();
            repo.UpdateCourse(course);
        }

        public List<CourseDto> GetAllCourses()
        {
            var repo = new CourseRepository();
            return repo
                .GetCourses()?
                .Select(FromEntityToDtoMapper.EntityToCourseDto)
                .ToList();
        }

        public void SetCurrentCourseByIndex(int index)
        {
            if (index < 0)
            {
                throw new Exception("Indeks nie moze byc liczba ujemna!");
            }

            var courses = GetAllCourses();
            if (courses.Count <= index)
            {
                throw new Exception("Nie ma takiego kursu!");
            }

            _currentCourseDto = courses[index];
        }

        public CourseDto GetCurrentCourseDto()
        {
            return _currentCourseDto;
        }

        public void AddStudentToCourse(StudentDto studentDto, CourseDto courseDto)
        {
            var repo = new CourseRepository();
            if (!repo.AddStudentToCourse(studentDto.Id,courseDto.Id))
                Console.WriteLine("Student o podanym peselu juz jest na kursie");
        }

        public void RemoveStudentFromCourse(StudentDto studentDto, CourseDto courseDto)
        {
            var repo = new CourseRepository();
            if (!repo.RemoveStudentFromCourse(studentDto.Id, courseDto.Id))
                Console.WriteLine("Student o podanym peselu nie jest na tym kursie");
            else
                Console.WriteLine("ok");
        }
    }
}