﻿using System;

namespace CourseLogicLayer
{
    public class CalculatingManagment
    {
        public static float PercentageCalculation(int firstArg, int secondArg)
        {
            float result;
            try
            {
                result = (firstArg / (float)secondArg)*100;
            
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return 0;
        }
    }
}