﻿using System;
using System.Collections.Generic;

namespace DataLayer.Models
{
    public class Course
    {
        // public string Course;
        public int Id { get; set; }

        public string CourseName { get; set; }
        public string TrainerName { get; set; }
        public DateTime CourseStartDate { get; set; }
        public int HomeworkThreshold { get; set; }
        public int PresenceThreshold { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public List<Homework> Homeworks { get; set; }
        public List<ClassDay> ClassDays { get; set; }

        public int AmountOfStudents { get; set; }
        public int AmountOfCourseDays { get; set; }

        //public virtual ICollection<Student> StudentsTable { get; set; }
        public Course()
        {
            Students = new List<Student>();
        }
    }
}