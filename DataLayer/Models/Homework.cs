﻿using System.Collections.Generic;

namespace DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public int RequiredPoints { get; set; }
        public Dictionary<Student, int> EarnedPoints { get; set; }
    }
}