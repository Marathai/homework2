﻿using System;
using System.Collections.Generic;

namespace DataLayer.Models
{
    public class ClassDay
    {
        public int Id { get; set; }
        public DateTime ClassDate { get; set; }
        public Dictionary<Student, bool> Presense { get; set; }
}
}
