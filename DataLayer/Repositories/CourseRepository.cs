﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataLayer.DbContexts;
using DataLayer.Models;

namespace DataLayer.Repositories
{
    public class CourseRepository
    {
        public List<Course> GetCourses()
        {
            List<Course> courses = null;
            using (var dbContext = new Context())
            {
                courses = dbContext.CoursesDbSet
                    .Include(c => c.Students)
                    .ToList();
            }
            return courses;
        }

        public bool AddCourse(Course course)
        {
            Course addedCourse = null;

            using (var dbContext = new Context())
            {
                addedCourse = dbContext.CoursesDbSet.Add(course);
                dbContext.SaveChanges();
            }

            return addedCourse != null;
        }

        public bool UpdateCourse(Course course)
        {
            using (var dbContext = new Context())
            {
                dbContext.CoursesDbSet.Attach(course);
                dbContext.Entry(course).State = EntityState.Modified;
                dbContext.SaveChanges();
                return true;
            }
        }

        public bool AddStudentToCourse(int studentId, int courseId)
        {
            using (var dbContext = new Context())
            {
                var courseFromDb = dbContext.CoursesDbSet
                    .Include(c => c.Students)
                    .Single(c => c.Id == courseId);

                var studentFromDb = dbContext.StudentsDbSet.Single(s => s.Id == studentId);

                if (courseFromDb.Students.Contains(studentFromDb))
                    return false;

                courseFromDb.Students.Add(studentFromDb);
                dbContext.SaveChanges();
                return true;
            }
        }

        public bool RemoveStudentFromCourse(int studentId, int courseId)
        {
            using (var dbContext = new Context())
            {
                var courseFromDb = dbContext.CoursesDbSet
                    .Include(c => c.Students)
                    .Single(c => c.Id == courseId);

                var studentFromDb = dbContext.StudentsDbSet.Single(s => s.Id == studentId);

                if (!courseFromDb.Students.Contains(studentFromDb))
                    return false;

                courseFromDb.Students.Remove(studentFromDb);
                dbContext.SaveChanges();
                return true;
            }
        }
    }
}