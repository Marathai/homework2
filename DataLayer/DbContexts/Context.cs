﻿using System;
using System.Configuration;
using System.Data.Entity;
using DataLayer.Models;


namespace DataLayer.DbContexts
{
    internal class Context : DbContext
    {
        public Context() : base(GetConnectionString())
        { }

        public DbSet<Student> StudentsDbSet { get; set; }
        public DbSet<Course> CoursesDbSet { get; set; }


        private static string GetConnectionString()
        {
            string cs = ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
            return cs;
        }

    }
}